/********************************************************************
*Created By : Dhriti Krishna Ghosh Moulick
*Created Date : 05/09/2018
*Description : Apex Helper Class on Contract object validating the Valid from and valid to Date.
			   If in database data exist with valid from and valid To date. Validation will fire
			   in insert or update if we are putting an overlapping valid from and valid to date.
*Modified By : 
*Modified Date :
*********************************************************************/
public class ContractTriggerHelper {
    public static void onBeforeInsert(List<Contract__c> onBeforeNewLst){
			validationOnOverlappingDate(onBeforeNewLst);
    }
    
    public static void onBeforeUpdate(List<Contract__c> onBeforeNewLst){
    		validationOnOverlappingDate(onBeforeNewLst);
        
    }
    public static void validationOnOverlappingDate(List<Contract__c> onBeforeNewLst){
         Map<String,List<Contract__c>> mapOfInsertedContract = new Map<String,List<Contract__c>>();
        
         mapOfInsertedContract = createMap(onBeforeNewLst); // Creating a map contains a key and list of similar records(This list contains the transactional data)
               
         List<Contract__c> contractLst = fetchExistingContractRecord(mapOfInsertedContract);  // Fetching the existing contract record
        
         Map<String,List<Contract__c>> mapOfExistingContractDiff = createMap(contractLst); // Creating a map contains a key and list of similar records(This list contains the existing data)
        
         validationRule(onBeforeNewLst,mapOfExistingContractDiff); // Calling validation Rule 
    }
	/********************************************************************

	*Description : Fetching the existing contract records

	*********************************************************************/
    public static List<Contract__c> fetchExistingContractRecord(Map<String,List<Contract__c>> mapOfInsertedContract){
        return [SELECT id,SubGradeSupplyType__c,Valid_From__c,Valid_To__c
                from Contract__c where SubGradeSupplyType__c in:mapOfInsertedContract.keyset() LIMIT 50000];
    }
    /********************************************************************

	*Description : logic to valid the overlapping date

	*********************************************************************/
    public static void validationRule(List<Contract__c> onBeforeNewLst,Map<String,List<Contract__c>> mapOfExistingContractDiff){
        
        for(Contract__c contrct : onBeforeNewLst){
            if(mapOfExistingContractDiff.containskey(contrct.SubGradeSupplyType__c)){
                List<Contract__c> cntrctLst = mapOfExistingContractDiff.get(contrct.SubGradeSupplyType__c);
                for(Contract__c ftDf : cntrctLst){

                    if(ftDf.Id <> contrct.Id){
                        if((contrct.Valid_From__c >= ftDf.Valid_From__c && contrct.Valid_From__c <= ftDf.Valid_To__c) ||
                                  (contrct.Valid_To__c >= ftDf.Valid_From__c && contrct.Valid_To__c <= ftDf.Valid_To__c)){
                                    
                                    contrct.addError('Cannot put overlapping date');
                        }
                    }
                }
            }
        }
    }
    /********************************************************************

	*Description : Creating a map contains Key as String and List of Values
				  (It is possible one key can contains multiple records)

	*********************************************************************/
    public static Map<String,List<Contract__c>> createMap(List<Contract__c> contractLst){
        
        Map<String,List<Contract__c>> mapOfInsertedCntrct= new Map<String,List<Contract__c>>();
        for(Contract__c contrct: contractLst){
            if(!mapOfInsertedCntrct.containsKey(contrct.SubGradeSupplyType__c)){
                mapOfInsertedCntrct.put(contrct.SubGradeSupplyType__c, new List<Contract__c>{contrct});
            }else{
                List<Contract__c> marginDtaLst = mapOfInsertedCntrct.get(contrct.SubGradeSupplyType__c);
                marginDtaLst.add(contrct);
                mapOfInsertedCntrct.put(contrct.SubGradeSupplyType__c,marginDtaLst);
            }
        }
        return mapOfInsertedCntrct;
    }
}